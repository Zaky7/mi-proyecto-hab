CREATE DATABASE IF NOT EXISTS VIAJES_DIFERENTES; 

USE VIAJES_DIFERENTES;

CREATE TABLE IF NOT EXISTS usuarios (
  id INT PRIMARY KEY auto_increment,
  nombre VARCHAR(255) NOT NULL,
  apellidos VARCHAR(255) NOT NULL,
  fecha_nacimiento DATETIME,
  alias VARCHAR(40) NOT NULL,
  email VARCHAR(40) NOT NULL,
  contraseña VARCHAR (20) NOT NULL,
  lugares_visitados VARCHAR (1000),
  foto_perfil VARCHAR (255),
  fecha_creacion_tabla DATETIME,
  fecha_modificacion_tabla DATETIME
);
CREATE TABLE IF NOT EXISTS lugares_experiencias (
  id INT PRIMARY KEY auto_increment,
  localizacion VARCHAR (255) NOT NULL,
  fotos VARCHAR (8000),
  pais VARCHAR (255) NOT NULL,
  valoracion_media VARCHAR (10),
  enclaves_de_interes VARCHAR (8000),
  fecha DATETIME,
  fecha_creacion_tabla DATETIME,
  fecha_modificacion_tabla DATETIME
 );

CREATE TABLE IF NOT EXISTS usuarios_lugares_experiencia (
  id INT PRIMARY KEY auto_increment,
  id_usuario INT,
  INDEX (id_usuario),
  FOREIGN KEY (id_usuario) REFERENCES usuarios (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  id_lugar_experiencia INT,
  INDEX (id_lugar_experiencia),
  FOREIGN KEY (id_lugar_experiencia) REFERENCES lugares_experiencias (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  fecha_creacion_tabla DATETIME,
  fecha_modificacion_tabla DATETIME
);

CREATE TABLE IF NOT EXISTS valoracion_lugares (
  id INT PRIMARY KEY auto_increment,
  criterio_1 ENUM ('1','2','3','4','5','6','7','8','9','10'),
  criterio_2 ENUM ('1','2','3','4','5','6','7','8','9','10'),
  criterio_3 ENUM ('1','2','3','4','5','6','7','8','9','10'),
  criterio_4 ENUM ('1','2','3','4','5','6','7','8','9','10'),
  valoracion_global ENUM ('1','2','3','4','5','6','7','8','9','10'),
  comentario VARCHAR(255),
  id_lugar INT,
  INDEX (id_lugar),
  FOREIGN KEY (id_lugar) REFERENCES lugares_experiencias (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  fecha_creacion_tabla DATETIME,
  fecha_modificacion_tabla DATETIME
);
CREATE TABLE IF NOT EXISTS usuarios_valoracion_lugares(
  id INT PRIMARY KEY auto_increment,
  id_usuario INT,
  INDEX (id_usuario),
  FOREIGN KEY (id_usuario) REFERENCES usuarios (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  id_valoracion_lugar INT,
  INDEX (id_valoracion_lugar),
  FOREIGN KEY (id_valoracion_lugar) REFERENCES valoracion_lugares (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  fecha_creacion_tabla DATETIME,
  fecha_modificacion_tabla DATETIME
);
